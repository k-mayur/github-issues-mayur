import React, { Component } from "react";
import classes from "./Issues.css";
import Header from '../../components/Header/Header';
import ListIssue from '../../components/ListIssue/ListIssue';
import IssueHeader from '../../components/IssueHeader/IssueHeader';
import ReactAux from '../../hoc/ReactAux';
import ReactPaginate from 'react-paginate';
import $ from 'jquery';

class IssuesGit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      issues: [],
      newIssues : null,
      err : '',
      page : 1,
      loading : false
    };
    this.url = 'https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues?page=';
  }

  loadIssuesFromServer(page) {
    $.ajax({
      url: `${this.url}${page}`,
      dataType: 'json',
      type: 'GET',

      success: data => {
        this.setState({
          issues : data,
          loading : false
        });
      },

      beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Token 8433fd16d93e2948c04efecb82f1555f7f432dd9'); },

      error: (xhr, status, err) => {
        console.error(this.url, status, err.toString()); // eslint-disable-line
        this.setState({err : err});
      },
    });
  }

  componentDidMount() {
    this.loadIssuesFromServer(this.state.page);
  }

  handlePageClick = data => {
    console.log(data.selected + 1);

    this.setState(prevState => {
      return {page : data.selected + 1, loading : true}
    }, () => {
      this.loadIssuesFromServer(this.state.page);
    }, () => {
      this.setState({loading : false});
    })
  };

  clearHandler = () => {
    this.setState({newIssues : null});
  }

  sortClickHandler = sort => {
    this.setState(prevState => {
      let newTemp = [];
      if (sort === 'newest') {
        newTemp = prevState.issues.sort((b, a) => {
          return new Date(a.created_at).getTime() - new Date(b.created_at).getTime();
        });      
      } else if (sort === 'oldest') {
        newTemp = prevState.issues.sort((a, b) => {
          return new Date(a.created_at).getTime() - new Date(b.created_at).getTime();
        });
      } else if (sort === 'recently updated') {
        newTemp = prevState.issues.sort((b, a) => {
          return new Date(a.updated_at).getTime() - new Date(b.updated_at).getTime();
        });
      } else if (sort === 'least recently updated') {
        newTemp = prevState.issues.sort((a, b) => {
          return new Date(a.updated_at).getTime() - new Date(b.updated_at).getTime();
        });
      }
      return {newIssues : newTemp};
    })
  }
  
  searchHandler = (e, value) => {
    if (e.key === 'Enter' && e.target.value.trim() !== '') {
      this.setState(prevState => {
        const newTemp = prevState.issues.filter(issue => issue.title.toLowerCase().includes(value.toLowerCase()) === true);
        return {newIssues : newTemp};
      });
      e.target.value = null;
    }    
  }

  authorHandler = author => {
    this.setState((prevState) => {
      const newTemp = prevState.issues.filter(issue => issue.user.login === author);
      return {newIssues : newTemp};
    })
  }

  labelHandler = label => {
    this.setState(prevState => {
      const newTemp = prevState.issues.filter(issue => {
        let newIssue = issue.labels.filter(lab => lab.name === label);
        if(newIssue.length > 0) {
          return true;
        } else {
          return false;
        }
      });
      return {newIssues : newTemp};
    });
  }

  stateHandler = state => {
    this.setState(prevState => {
      const newTemp = prevState.issues.filter(issue => issue.state === state);
      return {newIssues : newTemp}
    });
  }

  render() {
    if (this.state.issues.length > 0) {
      const { issues, newIssues } = this.state;

      let Issues;
      if (!this.state.loading) {
        if (this.state.newIssues === null) {
          Issues = issues.map((issue, i) => {
            return <ListIssue issue={issue} key={i}/>
          });
        } else if (this.state.newIssues.length === 0) {
          Issues = <div className={classes.empty} key='0'><h3>Nothing to Show</h3></div>
        } else {
          Issues = newIssues.map((issue, i) => {
            return <ListIssue issue={issue} key={i}/>
          });
        }
      } else {
        Issues = <div className={classes.loader}>Loading...</div>
      }
      
      
      return (
        <div className={classes.App}>
          <Header val={this.state.issues.length}/>
          <IssueHeader issues={this.state.issues} 
            click={this.sortClickHandler} 
            clear={this.clearHandler} 
            submit={this.searchHandler} 
            clickS={this.stateHandler} 
            clickA={this.authorHandler} 
            clickL={this.labelHandler}
          />
          <ReactAux>
            {Issues}
            <span className={classes.paginate}>
            <ReactPaginate
              previousLabel={'prev'}
              nextLabel={'next'}
              breakLabel={'..'}
              breakClassName={'break-me'}
              pageCount={10}
              marginPagesDisplayed={2}
              pageRangeDisplayed={5}
              onPageChange={this.handlePageClick}
              containerClassName={'pagination'}
              subContainerClassName={'pages pagination'}
              activeClassName={'active'}
            />
            </span>
            
          </ReactAux>  
        </div>
      );
    } else {
      return (<div><h3 className={classes.err}>{this.state.err}</h3><div className={classes.loader}>Loading...</div></div>)
    }    
  }
}

export default IssuesGit;
