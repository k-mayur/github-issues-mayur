import React, { Component} from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Issues from './containers/IssuesList/Issues';
import Issue from './containers/IssueSingle/Issue';
import Error from './components/Error/Error';

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path='/' component={Issues} exact/>
                    <Route path='/issues/:issueId' component={Issue} exact/>
                    <Route component={Error} />
                </Switch>
            </BrowserRouter>
        )
    }
}

export default App;