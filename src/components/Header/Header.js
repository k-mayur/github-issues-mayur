import React from 'react';
import classes from './Header.css';
import Icon from '@material-ui/core/Icon';

const header = props => {
    return (
        <div className={classes.header}>
            <h5>freeCodeCamp<span>&nbsp;/&nbsp;</span><strong>freeCodeCamp</strong></h5>
            <div className={classes.issue}>
                <Icon className={classes.exclamation}>error_outline</Icon>Issues <span className={classes.val}>{props.val}</span>
            </div>
        </div>
    );
}

export default header;